# README #

## Author: Taylor C. Brennan, tbrennan@uoregon.edu ##

### What is this repository for? ###

The objectives of this mini-project are:

  * Experience with GIT workflow with separate configuration: Fork the project, make and test changes locally, commit; turn in configuration file with reference to repo. (Project 0 is practice for this part.) 
  * Extend a tiny web server in Python, to check understanding of basic web architecture
  * Use automated tests to check progress (plus manual tests for good measure)

## Purpose ##
This is a simple pageserver designed to recieve a request and preform it. It creates a socket for the request and tries to access that location. This only handles "GET" requests from .html and .css file. This is to be used to understand how pageservers and requests are handled. This avoids the used of flask or simular solutions to the problem.

##Original##
-The pageserver is fully created, but all requests are sent to a picture of a cat. There are no file validation or paths for proper pages. This only handles "GET" requests

##Update - 2020/04/16##
-Still only handles "GET" requests
-When a request comes in:
  -Validate it is a "Get" otherwise sends proper error
  -Check for illeagal characters and sends propper error.
  -validate the request is the proper filetype and that it exists. Also gives the proper path for the file. Otherwise sends proper error.